/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class vehiculosMensajero extends AppCompatActivity {

    public TextView mCarro, mPlaca, mNombre;
    public Button mAtrasEnVehiuclo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiculos_mensajero);

        mCarro = (TextView) findViewById(R.id.vehiculoEnPantalla);
        mPlaca = (TextView) findViewById(R.id.placaEnPantalla);
        mNombre = (TextView) findViewById(R.id.nombreEnPantalla);

        mAtrasEnVehiuclo = (Button)findViewById(R.id.AtrasEnVehiculo);

        getDatosCarro();

        mAtrasEnVehiuclo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                return;
            }
        });

    }
    /**********************************************************************************************/
    private void getDatosCarro() {
        String IdMensajero = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero);

        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if (map.get("Vehiculo") != null) {
                        mCarro.setText("Tipo Vehiculo: " + map.get("Vehiculo").toString());
                    }
                    if (map.get("Matricula") != null) {
                        mPlaca.setText("Placa: " + map.get("Matricula").toString());
                    }
                    if(map.get("Nombre") != null){
                        mNombre.setText("Nombre: "+map.get("Nombre").toString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
