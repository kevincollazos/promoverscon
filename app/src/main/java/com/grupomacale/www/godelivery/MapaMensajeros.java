/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static java.lang.String.valueOf;

public class MapaMensajeros extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;


    private String customerId = "", Destino, Origen,Precio;
    private LatLng DestinoLatLng;

    private SupportMapFragment mapFragment;

    private Button aceptarPedido, rechazarPedido;

    private Button botonEntregado;

    DatabaseReference porcentajeref;

    private double saldo_global=0;

    public double porcent,mPrecioEnPerfil;

    DatabaseReference database, dineroRef , valorRef;

    private TextView saldoActualizado;

    private Boolean cerrandosesion = false;

    private String IdMensajero = FirebaseAuth.getInstance().getCurrentUser().getUid();

    private LinearLayout mInformacionCliente;

    private TextView mNombreCliente, mNumeroCliente, mDestinoCliente, mOrigenCliente, mPrecioEnvio;

    private TextView mtextSaldo, mNombreMensajero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
       setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hView = navigationView.getHeaderView(0);


        mInformacionCliente = (LinearLayout) findViewById(R.id.customerInfo);
        mOrigenCliente = (TextView) findViewById(R.id.OrigenEnvio);
        mPrecioEnvio = (TextView) findViewById(R.id.PrecioEnvio);
        mNombreCliente = (TextView) findViewById(R.id.customerName);
        mNumeroCliente = (TextView) findViewById(R.id.customerPhone);
        mDestinoCliente = (TextView) findViewById(R.id.customerDestination);

        aceptarPedido = (Button)findViewById(R.id.aceptarPedido);
        rechazarPedido=(Button)findViewById(R.id.rechazarPedido);

        mNombreMensajero = (TextView) hView.findViewById(R.id.nombreMensajeroMostrar);
        mtextSaldo = (TextView) hView.findViewById(R.id.saldoMensajero);

        getMensajeroInfo();
        getAssignedCustomer();

        aceptarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datospedido();
                Intent opcionAceptarPedido = new Intent(MapaMensajeros.this, fin.class);
                startActivity(opcionAceptarPedido);
                finish(); //ALEJANDRO
                return;
            }
        });

        rechazarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endRide();
                Intent intent = new Intent(MapaMensajeros.this, MapaMensajeros.class);
                finish();
                return;
            }
        });
    }
/*TRAER EL HISTORIAL DEL MENSAJERO SEGUN EL ID*/
    public void datospedido() {
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero).child("Peticion Cliente");

        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
         public void onDataChange(DataSnapshot dataSnapshot) {
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            mPrecioEnPerfil = Double.valueOf(map.get("Precio").toString());
//            porcent = Double.valueOf(map.get("Descuento").toString());
            Log.e("dato", valueOf(mPrecioEnPerfil)); //imprimir saldo en
            Log.e("dato", valueOf(porcent));
            datosusuario();
            finish();
         }
      @Override
         public void onCancelled(DatabaseError databaseError) {

                                                             }
         });
    }


/*************************************************************************************************************************/
public void datosusuario() {
    DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero);

    mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

            saldo_global=Double.valueOf(map.get("Saldo").toString());

            Log.e("dato", valueOf(saldo_global));//Imprimir el saldo en la consola
            finish();
        }


        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });
}
/************************************************************************************************************************/
public void actualizar_saldo(){
    porcentajeref.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            porcent = dataSnapshot.getValue(double.class);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });
    dineroRef.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            double value = dataSnapshot.getValue(double.class);
            saldo_global=value;
            Log.e("SALDO", valueOf(saldo_global));
            //   saldoActualizado.setText(String.valueOf(saldo_global));
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });
}

/*MUESTRA EL NOMBRE Y EL SALDO DEL MENSAJERO EN EL DASHBOARD*/

    private void getMensajeroInfo(){
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero);

        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("Nombre")!=null){
                        mNombreMensajero.setText(map.get("Nombre").toString());
                    }
                    if(map.get("Saldo")!=null){
                        mtextSaldo.setText("Mi saldo es: $"+map.get("Saldo").toString());

                        saldo_global=Double.valueOf(map.get("Saldo").toString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
/*OBTIENE EL LUGAR DEL MENSAJERO*/
    private void getAssignedCustomer(){
        DatabaseReference assignedCustomerRef = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero).child("Peticion Cliente").child("ID Envio");
        assignedCustomerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    customerId = dataSnapshot.getValue().toString();
                    getAssignedCustomerPickupLocation();
                    getAssignedCustomerDestination();
                    getAssignedCustomerInfo();
                }else{

                    //getAssignedCustomerPickupLocation(); // logica inversa
                    //getAssignedCustomerDestination();// logica inversa
                    //getAssignedCustomerInfo();// logica inversa
                    endRide();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /*********************************************************************************************************************/

    Marker pickupMarker;
    private DatabaseReference assignedCustomerPickupLocationRef;
    private ValueEventListener assignedCustomerPickupLocationRefListener;
    private void getAssignedCustomerPickupLocation(){
        assignedCustomerPickupLocationRef = FirebaseDatabase.getInstance().getReference().child("Peticion Cliente").child(customerId).child("l");
        assignedCustomerPickupLocationRefListener = assignedCustomerPickupLocationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && !customerId.equals("")){
                    List<Object> map = (List<Object>) dataSnapshot.getValue();
                    double locationLat = 0;
                    double locationLng = 0;
                    if(map.get(0) != null){
                        locationLat = Double.parseDouble(map.get(0).toString());
                    }
                    if(map.get(1) != null){
                        locationLng = Double.parseDouble(map.get(1).toString());
                    }
                    LatLng pickupLatLng = new LatLng(locationLat,locationLng);
                    pickupMarker = mMap.addMarker(new MarkerOptions().position(pickupLatLng).title("Origen Envio").icon(bitmapDescriptorFromVector(MapaMensajeros.this, R.drawable.ic_partida_cliente)));

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
/**********************************Icono Partida****************************************************************/
private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
    Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_launcher_background);
    background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
    Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
    vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth() + 0, vectorDrawable.getIntrinsicHeight() + 0);
    Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    background.draw(canvas);
    vectorDrawable.draw(canvas);
    return BitmapDescriptorFactory.fromBitmap(bitmap);
}
/**************************************************************************************************/
    Marker mMarcadorDestino;
    private void getAssignedCustomerDestination(){
        DatabaseReference assignedCustomerRef = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero).child("Peticion Cliente");

        assignedCustomerRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    Destino = "";
                    Origen="";
                    Precio="";
                    if(map.get("Destino")!=null){
                        Destino = map.get("Destino").toString();
                        mDestinoCliente.setText("Destino: " + Destino);
                    }
                    else{
                        mDestinoCliente.setText("Destino: --");
                    }
                    if (map.get("Origen")!=null){
                        Origen = map.get("Origen").toString();
                        mOrigenCliente.setText("Origen: "+Origen);
                    }else{
                        mOrigenCliente.setText("Origen: --");
                    }
                    if (map.get("Precio")!=null){
                        Precio = map.get("Precio").toString();
                        mPrecioEnvio.setText("Precio: "+Precio);
                    }else{
                        mPrecioEnvio.setText("Precio: --");
                    }

                    Double LatDestino = 0.0;
                    Double LngDestino = 0.0;
                    if(map.get("Lat Destino") != null){
                        LatDestino = Double.valueOf(map.get("Lat Destino").toString());
                    }
                    if(map.get("Lng Destino") != null){
                        LngDestino = Double.valueOf(map.get("Lng Destino").toString());
                        DestinoLatLng = new LatLng(LatDestino, LngDestino);
                        mMarcadorDestino = mMap.addMarker(new MarkerOptions().position(DestinoLatLng).title("Destino Envio").icon(bitmapDescriptorFromVector(MapaMensajeros.this, R.drawable.ic_destino_pedido)));
                    }
                    if (Origen != "" && Precio!="" && Destino!=""){
                        mInformacionCliente.setVisibility(View.VISIBLE);

                    } else {
                       endRide();


                    }


                }
              //  mInformacionCliente.setVisibility(View.VISIBLE); // logica inversa
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    /**********************************************************************************************/
    private BitmapDescriptor bitmapDescriptorFromVector2(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_bdestino_pedido);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth() + 0, vectorDrawable.getIntrinsicHeight() + 0);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    /**********************************************************************************************/
    private void getAssignedCustomerInfo(){
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Clientes").child(customerId);
        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("Nombre")!=null){
                        mNombreCliente.setText(map.get("Nombre").toString());
                    }
                    if(map.get("Numero")!=null){
                        mNumeroCliente.setText(map.get("Numero").toString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    /******************************************************************************************************************/
    private void endRide(){
        mInformacionCliente.setVisibility(GONE);
        DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero).child("Peticion Cliente");
        driverRef.removeValue();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Peticion Cliente");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.removeLocation(customerId);
        customerId="";

        if(pickupMarker != null){
            pickupMarker.remove();
            mMarcadorDestino.remove();

        }
        if (assignedCustomerPickupLocationRefListener != null){
            assignedCustomerPickupLocationRef.removeEventListener(assignedCustomerPickupLocationRefListener);
        }

        mNombreCliente.setText("");
        mNumeroCliente.setText("");
        mDestinoCliente.setText("Destino: --");
        mPrecioEnvio.setText("Precio: --");
        mOrigenCliente.setText("Origen: --");
    }
    /*********************************************************************************************************************/
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);

    }
    /********************************************************************************************************************/
    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
    /**********************************************************************************************************************/
    @Override
    public void onLocationChanged(Location location) {
        if(getApplicationContext()!=null){

            mLastLocation = location;
            LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

            DatabaseReference refAvailable = FirebaseDatabase.getInstance().getReference("Mensajero Disponible");
            DatabaseReference refWorking = FirebaseDatabase.getInstance().getReference("Mensajero Trabajando");
            GeoFire geoFireAvailable = new GeoFire(refAvailable);
            GeoFire geoFireWorking = new GeoFire(refWorking);

            switch (customerId){
                case "":
                    geoFireWorking.removeLocation(IdMensajero);
                    geoFireAvailable.setLocation(IdMensajero, new GeoLocation(location.getLatitude(), location.getLongitude()));
                    break;

                default:
                    geoFireAvailable.removeLocation(IdMensajero);
                    geoFireWorking.setLocation(IdMensajero, new GeoLocation(location.getLatitude(), location.getLongitude()));
                    break;
            }
        }
    }
    /**********************************************************************************************/
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    /**********************************************************************************************/

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
    /**********************************************************************************************/
    private void disconnectDriver(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Mensajero Disponible");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.removeLocation(IdMensajero);
    }
    /***********************************************************************************************/
    @Override
    protected void onStart() {
        super.onStart();
        // if(isInternetAvailable()) {
        database = FirebaseDatabase.getInstance().getReference();
        dineroRef = database.child("Usuarios").child("Mensajeros").child(IdMensajero).child("Saldo");
        porcentajeref =  database.child("Peticion Cliente").child("Descuento");
    }

    /**********************************************************************************************/

    @Override
    protected void onStop() {
        super.onStop();
        if (cerrandosesion){
            disconnectDriver();
        }

    }
    /**********************************************************************************************/
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    /**********************************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    /**********************************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /**********************************************************************************************/
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            Intent perfilMensajero = new Intent(MapaMensajeros.this, pedidosMensajeros.class);
            startActivity(perfilMensajero);

        } else if (id == R.id.nav_gallery) {

            Intent vehiculoMensajero = new Intent(MapaMensajeros.this, vehiculosMensajero.class);
            startActivity(vehiculoMensajero);


        } else if (id == R.id.inicio) {

            Intent mapaMensajero = new Intent(MapaMensajeros.this, perfil_mensajero.class);
            startActivity(mapaMensajero);

        } else if (id == R.id.salir) {
            cerrandosesion = true;

            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(MapaMensajeros.this, IngresoMensajero.class);
            startActivity(intent);
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    /**********************************************************************************************/
}
