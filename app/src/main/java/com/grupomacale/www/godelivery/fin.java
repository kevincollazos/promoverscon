/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.bumptech.glide.Glide;
import com.firebase.geofire.GeoFire;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Map;
import android.net.Uri;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.grupomacale.www.godelivery.MapaMensajeros;

import static android.view.View.GONE;


public class fin extends AppCompatActivity {


    private TextView mNombreCliente, mNumeroCliente, mDestinoCliente, mOrigenCliente, mPrecioEnvio;
    private String customerId = "", Destino, Origen,Precio;
    private TextView mNombre,  mTelefono;
    private FirebaseAuth mAuth;
    private DatabaseReference mBaseDatosMensajero;

    private MapaMensajeros datosMapa;

    private String userID;
    private String mTelefonoST;
    private String mProfileImageUrl;

    private Button mBTNACEPTAR;
    private Uri resultUri;
    private RadioGroup mRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin);


        mNombreCliente = (TextView) findViewById(R.id.Nombre_FIN);
        mNumeroCliente = (TextView) findViewById(R.id.Celular_FIN);
        mOrigenCliente = (TextView) findViewById(R.id.Origen_FIN);
        mDestinoCliente = (TextView) findViewById(R.id.Destino_FIN);
        mPrecioEnvio=(TextView) findViewById(R.id.Precio_FIN);

        mBTNACEPTAR = (Button) findViewById(R.id.BTN_ACEPTAR);


        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mBaseDatosMensajero = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(userID).child("Peticion Cliente");

        getInformacion();



        mBTNACEPTAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endRide();
                Intent pedidoTerminado= new Intent(fin.this, MapaMensajeros.class);
                startActivity(pedidoTerminado);
                finish();
                return;

            }
        });
    }

    private void getInformacion(){


        mBaseDatosMensajero.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                    if(map.get("Nombre")!=null){
                        mNombreCliente.setText("Nombre: "+map.get("Nombre").toString());
                    }
                    if(map.get("Numero")!=null){
                        mTelefonoST = map.get("Numero").toString();
                        mNumeroCliente.setText(mTelefonoST);
                    }

                    if(map.get("Origen")!=null){
                        mOrigenCliente.setText("Origen: "+map.get("Origen"));
                    }
                    if (map.get("Destino")!=null){
                        mDestinoCliente.setText("Destino: "+map.get("Destino"));
                    }
                    if (map.get("Precio")!=null){
                        Precio = map.get("Precio").toString();
                        mPrecioEnvio.setText("Precio: "+Precio);
                    }else{
                        mPrecioEnvio.setText("Precio: --");
                    }

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void endRide(){
        DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(userID).child("Peticion Cliente");
        driverRef.removeValue();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Peticion Cliente");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.removeLocation(userID);

        if (ref != null){
            ref.removeValue();
        }
        userID="";
        mNombreCliente.setText("");
        mNumeroCliente.setText("");
        mDestinoCliente.setText("");
        mPrecioEnvio.setText("");
        mOrigenCliente.setText("");
    }
}
