/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class perfil_mensajero extends AppCompatActivity {

    private TextView mNombre, mCorreo;

    private EditText mTelefono, mMatricula;

    private Button mAtras, mActualizar;

    private ImageView mProfileImage;

    private FirebaseAuth mAuth;
    private DatabaseReference mBaseDatosMensajero;

    private String userID;
    private String mNombreST;
    private String mCorreoST;
    private String mTelefonoST;
    private String mMatriculaST;
    private String mVehiculo;
    private String mProfileImageUrl;

    private Uri resultUri;

    private RadioGroup mRadioGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_mensajero);


        mNombre = (TextView) findViewById(R.id.Nombre);
        mCorreo = (TextView)findViewById(R.id.Email);
        mTelefono = (EditText) findViewById(R.id.Telefono);
        mMatricula = (EditText) findViewById(R.id.Matricula);

        mProfileImage = (ImageView) findViewById(R.id.profileImage);

        mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        mRadioGroup.check(R.id.Moto);

        mAtras = (Button) findViewById(R.id.Atras);
        mActualizar = (Button) findViewById(R.id.Actualizar);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mBaseDatosMensajero = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(userID);

        getInformacion();

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });

        mActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarInformacion();
            }
        });

        mAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                return;
            }
        });
    }
    private void getInformacion(){
        mBaseDatosMensajero.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("Nombre")!=null){
                        mNombre.setText(map.get("Nombre").toString());
                    }
                    if(map.get("Email")!=null){
                        mCorreo.setText(map.get("Email").toString());
                    }
                    if(map.get("Numero")!=null){
                        mTelefonoST = map.get("Numero").toString();
                        mTelefono.setText(mTelefonoST);
                    }
                    if(map.get("Matricula")!=null){
                        mMatriculaST = map.get("Matricula").toString();
                        mMatricula.setText(mMatriculaST);
                    }
                    if(map.get("Vehiculo")!=null){
                        mVehiculo = map.get("Vehiculo").toString();
                        switch (mVehiculo){
                            case"Moto":
                                mRadioGroup.check(R.id.Moto);
                                break;
                            case"Carro":
                                mRadioGroup.check(R.id.Carro);
                                break;
                            case"Bicicleta":
                                mRadioGroup.check(R.id.Bicicleta);
                                break;
                        }
                    }
                    if(map.get("profileImageUrl")!=null){
                        mProfileImageUrl = map.get("profileImageUrl").toString();
                        Glide.with(getApplication()).load(mProfileImageUrl).into(mProfileImage);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    private void GuardarInformacion() {
        mNombreST = mNombre.getText().toString();
        mTelefonoST = mTelefono.getText().toString();
        mMatriculaST = mMatricula.getText().toString();

        int selectId = mRadioGroup.getCheckedRadioButtonId();

        final RadioButton radioButton = (RadioButton) findViewById(selectId);

        if (radioButton.getText() == null){
            return;
        }

        mVehiculo = radioButton.getText().toString();

        Map userInfo = new HashMap();
        userInfo.put("Nombre", mNombreST);
        userInfo.put("Numero", mTelefonoST);
        userInfo.put("Matricula", mMatriculaST);
        userInfo.put("Vehiculo", mVehiculo);
        mBaseDatosMensajero.updateChildren(userInfo);

        if(resultUri != null) {

            StorageReference filePath = FirebaseStorage.getInstance().getReference().child("profile_images").child(userID);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            byte[] data = baos.toByteArray();
            UploadTask uploadTask = filePath.putBytes(data);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    finish();
                    return;
                }
            });
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();

                    Map newImage = new HashMap();
                    newImage.put("profileImageUrl", downloadUrl.toString());
                    mBaseDatosMensajero.updateChildren(newImage);

                    finish();
                    return;
                }
            });
        }else{
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            final Uri imageUri = data.getData();
            resultUri = imageUri;
            mProfileImage.setImageURI(resultUri);
        }
    }
}
