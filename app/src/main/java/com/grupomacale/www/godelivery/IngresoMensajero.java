/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.net.InetAddress;
import java.util.regex.Pattern;

public class IngresoMensajero extends AppCompatActivity {

    private EditText mCorreo, mContraseña;
    private Button mIngresar, mRegistrarse, mPass;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private Task<Void> mBaseDeDatosCliente;
    private FirebaseUser User;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso_mensajero);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        //Verificación de internet

        if (networkInfo != null && networkInfo.isConnected()) {



        } else {

            //Si no hay internet

            AlertDialog.Builder Sin_Internet = new AlertDialog.Builder(IngresoMensajero.this);
            Sin_Internet.setTitle("Sin conexión");
            Sin_Internet.setMessage("Revisa tu conexión a inernet e intenta \nnuevamente.");
            Sin_Internet.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    System.exit(0);

                }

            });

            Sin_Internet.show();

        }


        mAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser Usuario = FirebaseAuth.getInstance().getCurrentUser();
            }
        };


        mCorreo= (EditText) findViewById(R.id.Correo);
        mContraseña=(EditText) findViewById(R.id.Contraseña);
        mIngresar=(Button) findViewById(R.id.Ingresar);
        mRegistrarse=(Button)findViewById(R.id.Registrarse);
        mPass =(Button)findViewById(R.id.OlvidoPass);

        mRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent (IngresoMensajero.this, RegistroMensajeros.class);
                startActivity(intent);
                finish();
                return;
                 }
        });

        mIngresar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(mCorreo.getText().toString().trim().equals("")) {

                    mCorreo.setError("Por favor digite su correo.");
                    mCorreo.requestFocus();

                }else if(mContraseña.getText().toString().trim().equals("")){

                    mContraseña.setError("Por favor digite su contraseña.");
                    mContraseña.requestFocus();

                } else if (!Validar_Email(mCorreo.getText().toString())){

                    mCorreo.setError("Email no válido.");
                    mCorreo.requestFocus();

                }else if(mCorreo.getText().toString().trim().length() > 0 && mContraseña.getText().toString().trim().length()>0) {

                    final String email = mCorreo.getText().toString();

                    final String contraseña = mContraseña.getText().toString();

                    mAuth.signInWithEmailAndPassword(email, contraseña).addOnCompleteListener(IngresoMensajero.this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (!task.isSuccessful()) {

                                Toast.makeText(IngresoMensajero.this, "Error de ingreso.", Toast.LENGTH_SHORT).show();

                            }else {

                                //Dar valor al usuario actual

                                User = mAuth.getCurrentUser();

                                //Verifica si el usuario ya reviso su correo

                                if(!User.isEmailVerified()){

                                    //Aviso que no verifico

                                    AlertDialog.Builder Auten = new AlertDialog.Builder(IngresoMensajero.this);

                                    Auten.setTitle("Correo no verificado");
                                    Auten.setMessage("Por favor verifica tu correo electrónico.");
                                    Auten.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                        }
                                    });
                                    Auten.show();
                                }else{

                                    //Si ya verifico el correo

                                    mBaseDeDatosCliente = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Clientes").child(User.getUid()).child("Contraseña").setValue(mContraseña.getText().toString().trim());
                                    Intent intent = new Intent(IngresoMensajero.this, MapaMensajeros.class);
                                    startActivity(intent);
                                    finish();
                                    return;

                                }

                            }

                        }
                    });

                } else{

                    Toast.makeText(IngresoMensajero.this,"Por favor llene los datos solicitados.",Toast.LENGTH_SHORT).show();

                }

            }

        });


        mPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IngresoMensajero.this, recuperarPass.class);
                startActivity(intent);
                finish();
                return;
            }
        });
    }

    private boolean Validar_Email(String email) {

        Pattern pattern = Patterns.EMAIL_ADDRESS;

        return pattern.matcher(email).matches();

    }

    /**********************************************************************************************/
    @Override
    protected void onStart() {
        super.onStart();
            mAuth.addAuthStateListener(firebaseAuthListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.addAuthStateListener(firebaseAuthListener);
    }
    /**********************************************************************************************/
}
