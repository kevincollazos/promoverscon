/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistroMensajeros extends AppCompatActivity {

    private EditText mNombresMensajero, mApellidosMensajero, mEmailMensajero, mContraseñaMensajero, mNumeroMensajero,
            mDireccionMensajero,mEpsMensajero,mDocumentoMensajero, mContraseñaMensajeroVerificar;

    private Button mRegistroMensajero, mCerrarRegistroMensajero;


    private FirebaseAuth  mAuth;
    private FirebaseUser User;
    private DatabaseReference mBaseDeDatosMensajero;

    private Spinner mSpinnerCiudades;

    private String idMensajero;

    private String mNombreMensajeroDato, mApellidoMensajeroDato, mEmailMensajeroDato, mContraseñaMensajeroDato, mContraseñaMensajeroDatoVerificar,
            mNumeroMensajeroDato, mCiudadSpinnerDato, mDireccionMensajeroDato, mEpsMensajeroDato, mDocumentoMensajeroDato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_mensajeros);

        mNombresMensajero = (EditText) findViewById(R.id.NombreMensajero);
        mApellidosMensajero = (EditText) findViewById(R.id.ApellidosMensajero);
        mEmailMensajero = (EditText) findViewById(R.id.EmailMensajero);
        mContraseñaMensajero = (EditText) findViewById(R.id.ContraseñaMensajero);
        mNumeroMensajero = (EditText) findViewById(R.id.NumeroMensajero);
        mDireccionMensajero = (EditText)findViewById(R.id.direccionMensajeroRegistro);
        mEpsMensajero = (EditText)findViewById(R.id.epsMensajeroRegistro);
        mDocumentoMensajero = (EditText)findViewById(R.id.documentoMensajeroRegistro);
        mContraseñaMensajeroVerificar = (EditText)findViewById(R.id.ContraseñaMensajeroVerificar);

        mRegistroMensajero = (Button) findViewById(R.id.RegistrarMensajero);
        mCerrarRegistroMensajero = (Button) findViewById(R.id.CerrarRegistroMensajero);

        mSpinnerCiudades = (Spinner) findViewById(R.id.ciudadRegistro);


        mAuth = FirebaseAuth.getInstance();

        mRegistroMensajero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creacion de una alerta


                if (mNombresMensajero!=null && mApellidosMensajero!=null && mEmailMensajero!=null && mContraseñaMensajero!=null && mNumeroMensajero!=null && mDireccionMensajero != null && mEpsMensajero!=null && mDocumentoMensajero!=null && mContraseñaMensajeroVerificar!=null) {
                    if (!mNombresMensajero.equals("") && !mApellidosMensajero.equals("") && !mEmailMensajero.equals("") && !mContraseñaMensajero.equals("") && !mNumeroMensajero.equals("") && !mDireccionMensajero.equals("") && !mEpsMensajero.equals("") && !mDocumentoMensajero.equals("") && !mContraseñaMensajeroVerificar.equals("")) {

                        mNombreMensajeroDato = mNombresMensajero.getText().toString();
                        mApellidoMensajeroDato = mApellidosMensajero.getText().toString();
                        mEmailMensajeroDato = mEmailMensajero.getText().toString();
                        mContraseñaMensajeroDato = mContraseñaMensajero.getText().toString();
                        mNumeroMensajeroDato = mNumeroMensajero.getText().toString();
                            mCiudadSpinnerDato = mSpinnerCiudades.getSelectedItem().toString();
                        mDireccionMensajeroDato = mDireccionMensajero.getText().toString();
                        mEpsMensajeroDato = mEpsMensajero.getText().toString();
                        mDocumentoMensajeroDato = mDocumentoMensajero.getText().toString();
                        mContraseñaMensajeroDatoVerificar = mContraseñaMensajeroVerificar.getText().toString();

                        if (!Validar_Email(mEmailMensajeroDato)) {
                            Toast.makeText(RegistroMensajeros.this, "Direccion de correo invalida", Toast.LENGTH_SHORT).show();
                            mEmailMensajero.requestFocus();

                        } else {

                            if(!mContraseñaMensajeroDato.equals(mContraseñaMensajeroDatoVerificar)) {

                                Toast.makeText(RegistroMensajeros.this, "La contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                                mContraseñaMensajero.requestFocus();


                            }else {
                                if (mContraseñaMensajeroDato.length() < 5 && !isValidPassword(mContraseñaMensajeroDato)) {
                                    Toast.makeText(RegistroMensajeros.this, "La contraseña debe ser mayor a 5 caracteres", Toast.LENGTH_SHORT).show();
                                    mContraseñaMensajero.requestFocus();


                                } else {

                                    final String email = mEmailMensajero.getText().toString();
                                    final String contraseña = mContraseñaMensajero.getText().toString();
                                    mAuth.createUserWithEmailAndPassword(email, contraseña).addOnCompleteListener(RegistroMensajeros.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(RegistroMensajeros.this, "error de registro", Toast.LENGTH_SHORT).show();

                                            } else {
                                                idMensajero = mAuth.getCurrentUser().getUid();
                                                User = mAuth.getCurrentUser();
                                                User.sendEmailVerification();
                                                mBaseDeDatosMensajero = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(idMensajero);
                                                mBaseDeDatosMensajero.setValue(true);
                                                GuardarInformacionDelMensajero();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                        }else{
                            Toast.makeText(RegistroMensajeros.this, "Ningun campo debe estar vacio", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(RegistroMensajeros.this, "Por favor llena todos los datos", Toast.LENGTH_LONG).show();

                }
            }
        });


        mCerrarRegistroMensajero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistroMensajeros.this, IngresoMensajero.class);
                startActivity(intent);
                finish();
                return;
            }
        });

        DatabaseReference ciudades = FirebaseDatabase.getInstance().getReference().child("Ciudad");
        ciudades.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<String> ciudades = new ArrayList<String>();

                for (DataSnapshot areaSnapshot: dataSnapshot.getChildren()) {
                    String ciudadName = areaSnapshot.child("ciudad").getValue(String.class);
                    ciudades.add(ciudadName);
                    Log.e("Ciudades",ciudades.toString());
                }

                ArrayAdapter<String> ciudadesAdapter = new ArrayAdapter<String>(RegistroMensajeros.this, android.R.layout.simple_spinner_item, ciudades);
                ciudadesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mSpinnerCiudades.setAdapter(ciudadesAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private boolean Validar_Email(String email) {

        Pattern pattern = Patterns.EMAIL_ADDRESS;

        return pattern.matcher(email).matches();

    }

    private void GuardarInformacionDelMensajero(){


            Map InformacionDelUsuario = new HashMap();
            InformacionDelUsuario.put("Nombre", mNombreMensajeroDato.trim());
            InformacionDelUsuario.put("Apellido", mApellidoMensajeroDato.trim());
            InformacionDelUsuario.put("Email", mEmailMensajeroDato.trim());
            InformacionDelUsuario.put("Contraseña", mContraseñaMensajeroDato.trim());
            InformacionDelUsuario.put("Numero", mNumeroMensajeroDato.trim());
            InformacionDelUsuario.put("Saldo","0");
            InformacionDelUsuario.put("Direccion", mDireccionMensajeroDato.trim());
            InformacionDelUsuario.put("Ciudad", mCiudadSpinnerDato.trim());
            InformacionDelUsuario.put("Documento", mDocumentoMensajeroDato.trim());
            InformacionDelUsuario.put("Eps", mEpsMensajeroDato.trim());
            mBaseDeDatosMensajero.updateChildren(InformacionDelUsuario);

        AlertDialog.Builder Auten = new AlertDialog.Builder(RegistroMensajeros.this);

        Auten.setTitle("Verificar correo electrónico");
        Auten.setMessage("Por favor verifica tu correo para ser registrado satisfactoriamente.");
        Auten.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(RegistroMensajeros.this,IngresoMensajero.class);
                startActivity(intent);
                finish();
            }

        });

        Auten.show();

    }
    /*VALIDACION DE LA CONTRASEÑA*/
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.[0-9])(?=.[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}