/*
 * Desarrollado por Kevin Collazos, Juan Quiroga, Laura Rodriguez, Alejandro Acevedo, Ferney Rodriguez
 * Copyright (c) 2019. Todos los derechos reservados.
 */

package com.grupomacale.www.godelivery;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class pedidosMensajeros extends AppCompatActivity {

    LinearLayout historialDinamico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_mensajeros);

        historialDinamico= (LinearLayout)this.findViewById(R.id.historialDinamico);

        getIdHistorialMensajero();
    }
    /*TRAER EL ID DEL HISTORIAL DEL MENSAJERO*/

    private void getIdHistorialMensajero() {
        String IdMensajero = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference BaseDeDatosHistorialDelUsuario = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Mensajeros").child(IdMensajero).child("Historial");

        BaseDeDatosHistorialDelUsuario.addListenerForSingleValueEvent(new ValueEventListener() {
            int id=0;
          @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              if (dataSnapshot.exists()){
                    for(DataSnapshot Historial : dataSnapshot.getChildren()){
                        id = id+1;
                        FetchInformacionEnvio(Historial.getKey(),id);
                        Log.e("Historial ID", Historial.getKey()); //mostar en consola lo que trae el historial
                    }
                } else{
                  TextView pedidosVacios = new TextView(pedidosMensajeros.this);
                  pedidosVacios.setText("No Tienes Ningun Pedido");
                  pedidosVacios.setTextColor(Color.rgb(55,52,53));
                  pedidosVacios.setGravity(100);
                  historialDinamico.addView(pedidosVacios);
              }
          }

           @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    /*TRAER INFORMACION DEL HISTORIAL*/

    private void FetchInformacionEnvio(final String keyEnvio, final int id) {
        DatabaseReference BaseDeDatosHistorial= FirebaseDatabase.getInstance().getReference().child("Historial").child(keyEnvio);
        BaseDeDatosHistorial.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                TextView nombreHistorialCliente = new TextView(pedidosMensajeros.this);
                nombreHistorialCliente.setId(id);

                TextView destinoHistorial = new TextView(pedidosMensajeros.this);
                destinoHistorial.setId(id);

                TextView origenHistorial = new TextView(pedidosMensajeros.this);
                origenHistorial.setId(id);

                TextView precioHistorial = new TextView(pedidosMensajeros.this);
                precioHistorial.setId(id);
                
                    if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                        Log.e("DATOS HISTORIAL",map.get("Destino").toString());
                        if(map.get("Cliente")!=null){
                            nombreHistorialCliente.setText("Nombre Cliente: "+map.get("Cliente").toString());
                            nombreHistorialCliente.setPadding(12,15,0,12);
                            nombreHistorialCliente.setTextColor(Color.rgb(246,50,55));
                            historialDinamico.addView(nombreHistorialCliente);
                        }
                        if (map.get("Destino") != null) {
                            destinoHistorial.setText("Punto de llegada: " + map.get("Destino").toString());
                            destinoHistorial.setPadding(12,10,0,12);
                            destinoHistorial.setTextColor(Color.rgb(55,52,53));
                            historialDinamico.addView(destinoHistorial);
                        }
                        if (map.get("Origen")!=null){
                            origenHistorial.setText("Punto de partida: "+map.get("Origen").toString());
                            origenHistorial.setPadding(12,10,0,12);
                            origenHistorial.setTextColor(Color.rgb(55,52,53));
                            historialDinamico.addView(origenHistorial);
                        }
                        if (map.get("Precio")!=null){
                            precioHistorial.setText("Precio: "+map.get("Precio").toString());
                            precioHistorial.setPadding(12,10,0,12);
                            precioHistorial.setTextColor(Color.rgb(55,52,53));
                            historialDinamico.addView(precioHistorial);
                        }
                    }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
